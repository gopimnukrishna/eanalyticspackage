import sys
import time
import json
import re
from opcua import ua, Server, Client
import opcua.common
from kafka import KafkaProducer



# second client publishes the output in json format but only the values
# data_objects {
#    "namespace_list": [
#       {
#            "namespace": <value>,
#            "node_list": [
#               "DisplayName" : <value>,
#               "Variable_list": [
#                   {
#                       "CurrentValue": <value>
#                   }
#               ]
#           ]
#       }
#    ]
# }

def traverse_node(node, refs, nodeclassmask, dict):
    children = node.get_children(refs, nodeclassmask)
    # print (len(children))
    
    for child in range(0, len(children)):
        child_node = children[child]
        node_id = child_node.get_attribute(ua.AttributeIds.NodeId).Value.Value.to_string()
        if (node_id.startswith('ns=')):
            nsid_idx = re.findall(r"=[\d]+", node_id)
            
            nsid = int(nsid_idx[0][1:])
            idx = int(nsid_idx[1][1:])

            # print (child_node.get_node_class())
            if (child_node.get_node_class() == ua.NodeClass.Object):
                node_list_dict = dict['namespace_list'][nsid]['node_list']
            
                node_list_dict[idx] = {}
                node_dict = node_list_dict[idx]

                #node_dict['index'] = idx
                node_dict['DisplayName'] = child_node.get_display_name().to_string()
                node_dict['Variable_list'] = {}

                traverse_node(child_node, refs, 
                              ua.NodeClass.Variable,
                              node_dict['Variable_list'])
            elif (child_node.get_node_class() == ua.NodeClass.Variable):
                variables_dict = dict
                variable_name = child_node.get_display_name().to_string()
                variables_dict[variable_name] = {}
                current_value = opcua.common.ua_utils.variant_to_string(child_node.get_data_value())
                value = re.findall(r"Variant\(val:[a-zA-Z0-9\.]*", current_value)
                value = value[0][(len('Variant\(val:') - 1):]
                variables_dict[variable_name]['CurrentValue'] = value
                
                # print (re.findall(r"Variant\(val:[a-zA-Z0-9\.]*", variables_dict[variable_name]['CurrentValue']))

if __name__ == "__main__":

    # open a TCP connection to the server.
    client = Client("opc.tcp://localhost:4840/freeopcua/server/")
    try:
        client.connect()

        # Client has a few methods to get proxy to UA nodes that should always 
        # be in address space such as Root or Objects
        object = client.get_objects_node()
        # parsed_json = json.load(open('input.json'))

        object_schema ={}
        object_schema['namespace_list'] = {}
        uries = client.get_namespace_array()
        for uri in range(0, len(uries)):
            object_schema['namespace_list'][uri] = {}
            object_schema['namespace_list'][uri]['namespace'] = uries[uri];
            #object_schema['namespace_list'][uri]['index'] = uri;
            object_schema['namespace_list'][uri]['node_list'] = {}


        traverse_node(object, ua.ObjectIds.HierarchicalReferences, 
                      ua.NodeClass.Object, object_schema)
        print (object_schema)

        producer = KafkaProducer(bootstrap_servers='localhost:9092')


        for uri in object_schema['namespace_list']:
            prev_node_name = None
            for node in object_schema['namespace_list'][uri]['node_list']:
                sensor_object = object_schema['namespace_list'][uri]['node_list'][node]

                ns_name = object_schema['namespace_list'][uri]['namespace']
                node_name = object_schema['namespace_list'][uri]['node_list'][node]['DisplayName']

                if not prev_node_name:
                    prev_node_name = node_name
                    continue

                md = {}
                md['src_endpoint_id'] = ":".join(["opcua:", ns_name, node_name])
                md['dst_endpoint_id'] = ":".join(["opcua:", ns_name, prev_node_name]) 

                stream = {} 
                stream['md'] = md
                stream['data'] = sensor_object
                producer.send('input', json.dumps(stream))
                prev_node_name = node_name


        with open('stream.txt', 'w') as outfile:
            str_ = json.dumps(object_schema, indent=4, sort_keys=True,
                              separators=(',', ': '), ensure_ascii=False)
            outfile.write(str_)
        # Node objects have methods to read and write node attributes as well 
        # as browse or populate address space
        

        # num_entries = len(parsed_json['sensor_list'])
        # print("Found %d entries" % num_entries)
        # for entry in range(0, num_entries):
            # add sensors
        #    uri = parsed_json['sensor_list'][entry]['server_uri']
        #    add_more_sensors(client, uri, parsed_json['sensor_list'][entry])

    finally:
        client.disconnect()

