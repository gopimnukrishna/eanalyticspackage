import sys
sys.path.insert(0, "..")
import time
import json
import os


from opcua import ua, Server

# this function simulates number of sensors specified by count
# based on count number of nodes would be added in the namespace
def add_sensors(server, uri, entry):
    objects = server.get_objects_node()
    ns_index = server.get_namespace_index(uri)
    num_sensors = int(entry['count'])
    for index in range(0, num_sensors):
        # populating our address space
        myobj = objects.add_object(ns_index, str(entry['name_prefix'] + str(index)))
        myName = myobj.add_variable(ns_index, "Manufacturer", "Goofer")
        myvar = myobj.add_variable(ns_index, "CurrentValue", float(entry['value']))

        # Set MyVariable to be writable by clients
        myvar.set_writable()


def update_sensors(server, uri, entry):
    root = server.get_root_node()
    ns_index = server.get_namespace_index(uri)

    num_sensors = int(entry['count'])
    for index in range(0, num_sensors):
        myvar = root.get_child(["0:Objects", str(ns_index) + ":" + entry['name_prefix'] + str(index), str(ns_index) + ":" + "CurrentValue"])
        new_value = myvar.get_value() + (index + 1) * float(entry['increment'])
        if (new_value > float(entry['max_value'])):
            print ("Resetting value for %s%d" %(entry['name_prefix'], index))
            new_value = float(entry['value'])
        myvar.set_value(new_value)

# this function simulates number of sensors specified by count
# based on count number of nodes would be added in the namespace
#def add_rpm_sensors(server, count);

# this function simulates number of sensors specified by count
# based on count number of nodes would be added in the namespace
#def add_vibration_sensors(server, count);

if __name__ == "__main__":

    if  len(sys.argv) != 2:
      print "Usage: % <input file name>", sys.argv[0]
      exit(0)

    parsed_json = json.load(open(sys.argv[1]))
    # setup our server
    server = Server()
    # server.set_endpoint("opc.tcp://0.0.0.0:4840/freeopcua/server/")
    server.set_endpoint(parsed_json['server_end_point'])
    myhost = os.uname()[1]

    # populating our address space
    # for number of entries within list of parsed json create objects
    num_entries = len(parsed_json['sensor_list'])
    print("Found %d entries" % num_entries)
    for entry in range(0, num_entries):
        # add sensors
        uri = parsed_json['sensor_list'][entry]['server_uri'] +":"+ myhost
        server.register_namespace(uri)
        add_sensors(server, uri, parsed_json['sensor_list'][entry])

    # starting!
    server.start()
    
    try:
        start_time = 0
        while True:
            time.sleep(1)
            for entry in range(0, num_entries):
                # update sensors
                uri = parsed_json['sensor_list'][entry]['server_uri'] +":"+ myhost
                update_sensors(server, uri, parsed_json['sensor_list'][entry])

    finally:
        #close connection, remove subcsriptions, etc
        server.stop()
