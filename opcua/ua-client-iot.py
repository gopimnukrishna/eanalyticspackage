import sys
import time
import json
import re
from opcua import ua, Server, Client
import requests
import os


# this function simulates number of sensors specified by count
# based on count number of nodes would be added in the namespace
def add_more_sensors(client, uri, entry):
    objects = client.get_objects_node()
    ns_index = client.get_namespace_index(uri)

    num_sensors = int(entry['count'])
    for index in range(0, num_sensors):
        # populating our address space
        myobj = objects.add_object(ns_index, entry['name_prefix'] + "-new" + str(index))
        myName = myobj.add_variable(ns_index, "Manufacturer", "Goofer-new")
        myvar = myobj.add_variable(ns_index, "CurrentValue", float(entry['value']))

        # Set MyVariable to be writable by clients
        myvar.set_writable()
        
def traverse_node(node, refs, nodeclassmask, dict):
    children = node.get_children(refs, nodeclassmask)
    # print (len(children))
    
    for child in range(0, len(children)):
        child_node = children[child]
        node_id = child_node.get_attribute(ua.AttributeIds.NodeId).Value.Value.to_string()
        if (node_id.startswith('ns=')):
            nsid_idx = re.findall(r"=[\d]+", node_id)
            
            nsid = int(nsid_idx[0][1:])
            idx = int(nsid_idx[1][1:])

            # print (child_node.get_node_class())
            if (child_node.get_node_class() == ua.NodeClass.Object):
                node_list_dict = dict['namespace_list'][nsid]['node_list']
            
                node_list_dict[idx] = {}
                node_dict = node_list_dict[idx]

                #node_dict['index'] = idx
                node_dict['DisplayName'] = child_node.get_display_name().to_string()
                node_dict['Variable_list'] = {}

                traverse_node(child_node, refs, 
                              ua.NodeClass.Variable,
                              node_dict['Variable_list'])
            elif (child_node.get_node_class() == ua.NodeClass.Variable):
                variables_dict = dict
                variable_name = child_node.get_display_name().to_string()
                variables_dict[variable_name] = {}
                variables_dict[variable_name]['type'] = child_node.get_data_type_as_variant_type().__str__()

if __name__ == "__main__":

    # open a TCP connection to the server.
    client = Client("opc.tcp://localhost:4840/freeopcua/server/")
    myhost = os.uname()[1]
    local_sensor_cache = {}

    try:
        client.connect()

	while True:

            # Client has a few methods to get proxy to UA nodes that should always 
            # be in address space such as Root or Objects
            object = client.get_objects_node()

            object_schema ={}
            object_schema['namespace_list'] = {}
            uries = client.get_namespace_array()

            for uri in range(0, len(uries)):
                object_schema['namespace_list'][uri] = {}
                object_schema['namespace_list'][uri]['namespace'] = uries[uri];
                object_schema['namespace_list'][uri]['node_list'] = {}


            traverse_node(object, ua.ObjectIds.HierarchicalReferences,
                          ua.NodeClass.Object, object_schema)

            for uri in object_schema['namespace_list']:
                for node in object_schema['namespace_list'][uri]['node_list']:
                    sensor_object = object_schema['namespace_list'][uri]['node_list'][node]
                    ns_name = object_schema['namespace_list'][uri]['namespace']
                    node_name = object_schema['namespace_list'][uri]['node_list'][node]['DisplayName']
                    sensor_object['endpoint_id'] = ":".join(["opcua:", ns_name, node_name])
                    sensor_object['gw_name'] = myhost
                    sensor_object['access_proto'] = "opcua"
                    sensor_object['tags'] = {}

                    # Device tags.
                    sensor_object['tags']['device_tags'] = {
                        "namespace": ns_name,
                        "node": node_name
                    }

                    sensor_object['tags']['custom_tags'] = {
                        "uri": uri
                    }

                    # Are there any new sensors discovered?
                    if sensor_object['endpoint_id'] in local_sensor_cache:
                        continue

                    url = 'http://10.1.1.254:6543/endpoint_add'

                    r = requests.put(url, json=sensor_object)

                    local_sensor_cache[sensor_object['endpoint_id']] = sensor_object

            time.sleep(10)

    finally:
        client.disconnect()

