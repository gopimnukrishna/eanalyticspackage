#!/bin/bash

SERVER_IP=$1
SERVER_PORT=$2
POLICY_ENGINE_LOG_FILE=$3

: "${SERVER_IP:?"Need to set SERVER_IP env"}"
: "${SERVER_PORT:?"Need to set SERVER_PORT env"}"
: "${POLICY_ENGINE_LOG_FILE:?"Need to set POLICY_ENGINE_LOG_FILE env"}"

sudo touch /etc/rsyslog.d/apeirogw.conf
sudo chmod 666 /etc/rsyslog.d/apeirogw.conf
sudo cat > /etc/rsyslog.d/apeirogw.conf << EOM
module(load="imfile" PollingInterval="10") #needs to be done just once

# File 1
input(type="imfile"
      File="${POLICY_ENGINE_LOG_FILE}"
      Tag="apeiro_policyengine")

*.* @@${SERVER_IP}:${SERVER_PORT}
EOM

# Restart rsyslog service.
sudo service rsyslog restart
