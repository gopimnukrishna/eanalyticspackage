# python script to fake log messages for Kibana
# sample message
# <2018-01-06 03:59:46,557 gw3 DPI actuator-CHINA-allow-log_1 DENY SrcDevice: opcua:.http://dubbadubba.io.VibrationSensor15 DstDevice:  
# cpapp1 Labels: SrcLabels[device [device_family:actuator __geo_loc__:CHINA] network [] service []] DstLabels[device [] 
# network [] service []] Msg: dummy msg>
# corresponding grok
# match => { "message" => "%{TIMESTAMP_ISO8601} %{WORD:gateway} %{WORD:re_app} %{DATA:policy} %{WORD:action} SrcDevice\: %{GREEDYDATA:src} 
# DstDevice\: %{GREEDYDATA:dest} Labels\: SrcLabels\[device \[(?<srcDevTags>[a-zA-Z0-9_:-]*)\] network \[(?<srcNwTags>[a-zA-Z0-9_:-]*)\] 
# service \[(?<srcSvcTags>[a-zA-Z0-9_:-]*)\]\] DstLabels\[device \[(?<destDevTags>[a-zA-Z0-9_:-]*)\] 
# network \[(?<destNwTags>[a-zA-Z0-9_:-]*)\] service \[(?<destSvcTags>[a-zA-Z0-9_:-]*)\]\] Msg\: %{GREEDYDATA:msg}"
# 
import json
import random
from datetime import datetime
import datetime
import pprint

debug = 0
def debug_print(arg):
    if (debug):
        pprint.pprint(arg)

if __name__ == "__main__":

    parsed_json = json.load(open('log-input.json'))
    debug_print (parsed_json)

    # Logs are generated based on the following logic
    # for listed gateways
    #    for mentioned apps
    #      for src_label -> dest_label
    #         for range of time
    #            generate a message
    #
    #

    # action_list
    action = ['DENY', 'ALLOW', 'LOG', 'DENY+LOG', 'ALLOW+LOG']

    # get gateway list
    num_gateways = len(parsed_json['gateways'])

    debug_print("Found %d Gateways" % num_gateways)
    for gw_index in range(0, num_gateways):
        # get gateway name
        gw = parsed_json['gateways'][gw_index]
        gw_name = gw['name']

        # get end point name generation data
        src_ep_prefix_list = gw['src_ep_prefixes'].split()
        src_ep_prefix_count = len(src_ep_prefix_list)

        src_ep_dev_prefix_list = gw['src_ep_dev_prefixes'].split()
        src_ep_dev_prefix_count = len(src_ep_dev_prefix_list)

        src_ep_range = int(gw['src_ep_count'])

        dest_ep_prefix_list = gw['dest_ep_prefixes'].split()
        dest_ep_prefix_count = len(dest_ep_prefix_list)

        dest_ep_range = int(gw['dest_ep_count'])

        # get list of apps
        num_apps = len(gw['apps'])
        debug_print ("Found %d apps for GW %s" %(num_apps, gw_name))

        for app_idx in range(0, num_apps):
            # get app name
            app = gw['apps'][app_idx]
            app_name = app['app_name']

            # get list of policies
            num_policies = len(app['policies'])
            debug_print ("Found %d policies for app %s" %(num_policies, app_name))

            for policy_idx in range(0, num_policies):
                # get policy name
                policy = app['policies'][policy_idx]
                policy_name = policy['policy_name']

                # get src tags
                src_tags = policy['src_tags'].split('+')
                src_tags_count = len(src_tags)

                # get list of dest tags
                dest_tags = policy['dest_tags']
                debug_print ("Found %s src tag and %d dest tags for policy %s" %(src_tags,
                             len(dest_tags), policy_name))

                for dest_tag_idx in range(0, len(dest_tags)):
                    # get dest tag
                    dest_tag_name = dest_tags[dest_tag_idx]['dest_tag']
                    start_time = int(dest_tags[dest_tag_idx]['start_time'])
                    num_entries = int(dest_tags[dest_tag_idx]['num_entries'])
                    incr = int(dest_tags[dest_tag_idx]['incr'])

                    # build the final message
                    # <2018-01-06 03:59:46,557 gw3 DPI actuator-CHINA-allow-log_1 DENY 
                    # SrcDevice: opcua:.http://dubbadubba.io.VibrationSensor15 
                    # DstDevice: cpapp1 
                    # Labels: SrcLabels[device [device_family:actuator __geo_loc__:CHINA] network [] service []] 
                    # DstLabels[device [] network [] service []] 
                    # Msg: dummy msg>
                    debug_print (start_time)
                    time = start_time
                    
                    for entry in range(0, num_entries):
                        src_ep_name = src_ep_prefix_list[random.randint(1, src_ep_prefix_count) - 1] + \
                                      src_ep_dev_prefix_list[random.randint(1, src_ep_dev_prefix_count) - 1] + \
                                      str(random.randint(1, src_ep_range))
                        dest_ep_name = dest_ep_prefix_list[random.randint(1, dest_ep_prefix_count) - 1] + \
                                      str(random.randint(1, dest_ep_range))
                        src_tag = src_tags[random.randint(1, src_tags_count) - 1]

                        time_in_sec = datetime.datetime.fromtimestamp(time/1000)
                        print ("%s %s %s %s %s SrcDevice: %s DstDevice: %s Labels: "
                               "SrcLabels[device [%s] network [] service []] "
                               "DstLabels[device [%s] network [] service []] "
                               "Msg: dummy msg" %(time_in_sec.isoformat(), gw_name, app_name, 
                                policy_name, random.choice(action), src_ep_name, dest_ep_name,
                                src_tag, dest_tag_name))
                        time += incr
                        
            