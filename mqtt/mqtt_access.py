import paho.mqtt.publish as mqtt_publish
import paho.mqtt.client as mqtt

from kafka import KafkaConsumer
import json


if __name__ == "__main__":

    while True:

        kafka_consumer = KafkaConsumer('output',
                             group_id='my-group',
                             bootstrap_servers=['localhost:9092'],
    			 auto_offset_reset='smallest')

        for message in kafka_consumer:
        	try:
                	message_json = json.loads(message.value)
        	except:
        		print "Unsupported format"
        		continue
        	if 'md' not in message_json or 'dst_endpoint_id' not in message_json['md']:
        		print "Missing endpoint"
        		continue
        	try:
                	endpoint_id = message_json['md']['dst_endpoint_id']
                	endpoint_params = endpoint_id.split(':')
        		print "endpoints", endpoint_params[1],endpoint_params[2]
        		print "message ", message_json['data']
                	mqtt_publish.single(topic=endpoint_params[2], 
        			payload=json.dumps(message_json['data']),
        			hostname=endpoint_params[1], protocol=mqtt.MQTTv31)
        	except:
        		print "Error in publish message"
        		continue


