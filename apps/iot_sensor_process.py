#
# Licensed to the Apache Software Foundation (ASF) under one or more
# contributor license agreements.  See the NOTICE file distributed with
# this work for additional information regarding copyright ownership.
# The ASF licenses this file to You under the Apache License, Version 2.0
# (the "License"); you may not use this file except in compliance with
# the License.  You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

"""
 Counts words in UTF8 encoded, '\n' delimited text directly received from Kafka in every 2 seconds.
 Usage: direct_kafka_wordcount.py <broker_list> <topic>

 To run this on your local machine, you need to setup Kafka and create a producer first, see
 http://kafka.apache.org/documentation.html#quickstart

 and then run the example
    `$ bin/spark-submit --jars \
      external/kafka-assembly/target/scala-*/spark-streaming-kafka-assembly-*.jar \
      examples/src/main/python/streaming/direct_kafka_wordcount.py \
      localhost:9092 test`
"""
from __future__ import print_function

import sys
import json


from pyspark import SparkContext
from pyspark.streaming import StreamingContext
from pyspark.streaming.kafka import KafkaUtils

from pyspark.sql import SparkSession
from pyspark.sql.functions import explode
from pyspark.sql.functions import split
from pyspark.sql.functions import lit
from pyspark.sql.functions import struct
from pyspark.sql.functions import udf
from pyspark.sql.types import StructType, StructField, IntegerType, StringType, FloatType, TimestampType

json_schema = StructType([
  StructField("device id", StringType(), True),
  StructField("location", StringType(), True),
  StructField("timestamp", StringType(), True),
  StructField("temperature", FloatType(), True)
])

if __name__ == "__main__":

    print("Starting......")


    spark = SparkSession\
        .builder\
        .appName("StructuredNetworkWordCount")\
        .getOrCreate()

    # Create DataFrame representing the stream of input lines from connection to host:port
    data_df = spark\
        .readStream\
        .format("kafka")\
        .option("kafka.bootstrap.servers", "localhost:9092")\
        .option("subscribe", "test")\
        .load()

    # Split the lines into words
    jsonst = data_df.selectExpr("CAST(value AS STRING)")

    def myStrtoJson(json_str):
        try:
            json_val = json.loads(json_str)
            device_id = str(json_val['device id'])
            location = str(json_val['location'])
            timestamp = (json_val['timestamp'])
            temperature = (json_val['temperature'])


            return [device_id, location, timestamp, temperature]

        except ValueError:
            return

    myudf=(udf(myStrtoJson, json_schema))
    jsonst2 = jsonst.withColumn("mycolumn", myudf("value"))

    # Filter out rows that match a column value
    jsonst3 = jsonst2.filter(jsonst2.mycolumn.temperature > 45.0)

    query = jsonst3\
        .writeStream\
        .outputMode('append')\
        .format('console')\
        .start()

    # Start running the query that prints the running counts to the console
#   query = lines\
#       .writeStream\
#       .outputMode('append')\
#       .format('console')\
#       .start()


    query.awaitTermination()

