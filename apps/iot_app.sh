#!/bin/bash

source /home/vagrant/my_setup/bin/activate
/home/vagrant/eanalyticspackage/spark-2.2.0-bin-hadoop2.7/bin/spark-submit --packages org.apache.spark:spark-streaming-kafka_2.11:1.6.3,org.apache.spark:spark-sql-kafka-0-10_2.11:2.2.0 /home/vagrant/eanalyticspackage/apps/iot_sensor_process_output_kafka.py $1

