#!/bin/bash

start-stop-daemon --stop --quiet --retry=TERM/30/KILL/5 --pidfile /tmp/policy-engine.pid

exit 0
