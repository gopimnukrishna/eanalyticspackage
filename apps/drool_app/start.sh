#!/bin/bash

debug=true
usage="
$(basename "$0") -- program to start a Drools App.

where:
    -h  show this help text
    -b  kafka broker info in the format brokerip:port 
    -i  input kafka topic
    -o  output kafka topic
    -s  run in silent mode with no debug info
    -n  name of the app
"

# Default app name.
APPNAME="DPI"

while [[ $# -gt 0 ]]
do
key="$1"

case $key in
    -b|--broker)
    BROKER="$2"
    shift # past argument
    shift # past value
    ;;
    -i|--input-topic)
    INPUTTOPIC="$2"
    shift # past argument
    shift # past value
    ;;
    -o|--output-topic)
    OUTPUTTOPIC="$2"
    shift # past argument
    shift # past value
    ;;
    -n|--name)
    APPNAME="$2"
    shift # past argument
    shift # past value
    ;;
    -s| --silent)
    debug=false
    shift # past argument
    ;;
    -h| --help)
    echo "$usage" >&2
    exit 1
    ;;
    *)    # unknown option
    echo "Invalid arguments passed to program: $key"
    exit 1
    ;;
esac
done

if [ "$debug" = true ] ; then
    echo BROKER          = "${BROKER}"
    echo INPUT TOPIC     = "${INPUTTOPIC}"
    echo OUTPUT TOPIC    = "${OUTPUTTOPIC}"
    echo APP NAME        = "${APPNAME}"
fi

start-stop-daemon -m --start --quiet --pidfile /tmp/policy-engine.pid -b --startas /bin/bash -- -c "exec mvn -f ~/eanalyticspackage/apps/drool_app/pom.xml -X compile exec:java -Dexec.args='$BROKER $INPUTTOPIC $OUTPUTTOPIC $APPNAME'"

exit 0

