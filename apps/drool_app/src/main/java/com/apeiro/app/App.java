package com.apeiro.app;

import org.kie.api.KieServices;
import org.kie.api.runtime.KieContainer;
import org.kie.api.runtime.KieSession;

import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.ConsumerRebalanceListener;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.common.TopicPartition;

import org.apache.kafka.clients.producer.Callback;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.clients.producer.RecordMetadata;
import java.util.Random;
import java.util.Collection;
import java.util.Collections;
import java.util.Properties;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Arrays;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.google.gson.internal.LinkedTreeMap;
import java.lang.*;
import java.io.*;
import java.net.*;
import com.apeiro.app.Message;
import com.apeiro.app.Metadata;
import org.apache.log4j.*;
import com.apeiro.app.Action;
import com.mongodb.MongoClient;
import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import java.util.Iterator;
import java.util.Map;

public class App {

    public static Logger LOGGER;

    public static void Log(Level lvl, String msg) {

        // TBD: Check level and invoke corresponding logger call.

        LOGGER.info(msg);
    }

    public static String LogFormatter (String rule_name, Action action, String src, String dst, String labels, String msg) {
        if(labels == null || labels.isEmpty()) {
            labels = new String("SrcLabels[device [] network [] service []] DstLabels[device [] network [] service []]");
        }
        return String.format("%s %s SrcDevice: %s DstDevice: %s Labels: %s Msg: %s", rule_name, action.name(), src, dst, labels, msg);
    }

    private static HashMap<String, String> getTagHashMap (String device, DBObject obj, String type) {

        DBObject tdb_obj = (DBObject)obj.get(type);
        if (tdb_obj == null) {
            LOGGER.info("Tags of type " + type + " not found for device : " + device);
            return new HashMap<String, String>();
        }

        Map<String, Object> tag_obj = tdb_obj.toMap();

        HashMap<String, String> tag = new HashMap<String, String>();
        Iterator it = tag_obj.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry pair = (Map.Entry)it.next();
            tag.put((String)pair.getKey(), (String)pair.getValue());
            it.remove(); // avoids a ConcurrentModificationException
        }
        return tag;
    }

    public static void main(String[] args) {

        System.out.println("Program Arguments:");
        for (String arg : args) {
            System.out.println("\t" + arg);
        }

        String broker = args[0];
        String input_topic = args[1];
        String output_topic = args[2];
        String app_name = args[3];

        KieServices ks = KieServices.Factory.get();
        KieContainer kcontainer = ks.getKieClasspathContainer();
        KieSession ksession = kcontainer.newKieSession();

        Properties consumerConfig = new Properties();
        consumerConfig.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, broker);
        consumerConfig.put(ConsumerConfig.GROUP_ID_CONFIG, "my-group");
        consumerConfig.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "earliest");
        consumerConfig.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, "org.apache.kafka.common.serialization.StringDeserializer");
        consumerConfig.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, "org.apache.kafka.common.serialization.StringDeserializer");
        KafkaConsumer<String, String> consumer = new KafkaConsumer<>(consumerConfig);
        TestConsumerRebalanceListener rebalanceListener = new TestConsumerRebalanceListener();
        consumer.subscribe(Collections.singletonList(input_topic), rebalanceListener);

        Properties props = new Properties();
        props.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, broker);
        props.put(ProducerConfig.ACKS_CONFIG, "all");
        props.put(ProducerConfig.RETRIES_CONFIG, 0);
        props.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, "org.apache.kafka.common.serialization.StringSerializer");
        props.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, "org.apache.kafka.common.serialization.StringSerializer");
        Producer<String, String> producer = new KafkaProducer<String, String>(props);
        TestCallback callback = new TestCallback();

        // Configure and setup logger in working memory.
        try {
            System.setProperty("hostName", InetAddress.getLocalHost().getHostName());
        } catch (Throwable exception) {
        }
        LOGGER = Logger.getLogger(app_name);
        LOGGER.setLevel(Level.INFO);

        // Connect to Mongo DB
        System.out.println("Controller connection....");
        MongoClient mongo = new MongoClient( "10.1.1.254" , 27017 );
        DB db = mongo.getDB("iotdb");
        DBCollection table = db.getCollection("endpoint_tree");

        while (true) {
            ConsumerRecords<String, String> records = consumer.poll(1000);
            for (ConsumerRecord<String, String> record : records) {

                HashMap<String,Object> yourHash;
                try {
                    yourHash = new Gson().fromJson(record.value(), new TypeToken<HashMap<String, Object>>(){}.getType());
                } catch (Throwable exception) {
                    LOGGER.error("Unable to convert input string to JSON : " + record.value());
                    continue;
                }
                System.out.println(record.value());

                if (yourHash == null || !yourHash.containsKey("data") || !yourHash.containsKey("md")) {
                    LOGGER.error("Input string with invalid metadata " + record.value());
                    continue;
                }

                // Create notification blob.
                Message message = new Message(output_topic);
                message.setData(yourHash.get("data"));
                message.setMetadata(yourHash.get("md"));

                if (message.getDstUuid() == null || message.getDstUuid().isEmpty()) {
                    LOGGER.error("Destination device not found in metadata. Msg: " + record.value());
                    continue;
                }

                if (message.getSrcUuid() == null || message.getSrcUuid().isEmpty()) {
                    LOGGER.error("Source device not found in metadata. Msg: " + record.value());
                    continue;
                }

                BasicDBObject searchQuery = new BasicDBObject();
                searchQuery.put("proto_extn.endpoint_id", message.getSrcUuid());
                DBCursor from_cursor = table.find(searchQuery);

                while (from_cursor.hasNext()) {

                    // Get the entry read from DB.
                    DBObject obj = (DBObject)from_cursor.next();
                    Map<String,Object> device_obj_map = obj.toMap();

                    // Process device tags.
                    DBObject tobj = (DBObject)device_obj_map.get("tags");
                    message.setDeviceFromTag(getTagHashMap(message.getSrcUuid(), tobj, new String("device_tags")));
                    message.setNetworkFromTag(getTagHashMap(message.getSrcUuid(), tobj, "network_tags"));
                    message.setServiceFromTag(getTagHashMap(message.getSrcUuid(), tobj, "service_tags"));
                    message.setServiceFromTag(getTagHashMap(message.getSrcUuid(), tobj, "custom_tags"));

                }

                if (message.getDstUuid() != null) {
                    BasicDBObject toQuery = new BasicDBObject();
                    toQuery.put("proto_extn.endpoint_id", message.getDstUuid());
                    DBCursor to_cursor = table.find(searchQuery);

                    while (to_cursor.hasNext()) {

                        // Get the entry read from DB.
                        DBObject obj = (DBObject)to_cursor.next();
                        Map<String,Object> device_obj_map = obj.toMap();

                        // Process device tags.
                        DBObject tobj = (DBObject)device_obj_map.get("tags");
                        message.setDeviceToTag(getTagHashMap(message.getDstUuid(), tobj, "device_tags"));
                        message.setNetworkToTag(getTagHashMap(message.getDstUuid(), tobj, "network_tags"));
                        message.setServiceToTag(getTagHashMap(message.getDstUuid(), tobj, "service_tags"));
                        message.setServiceToTag(getTagHashMap(message.getDstUuid(), tobj, "custom_tags"));

                    }
                }


                // Insert message to working memory for rule evaluation.
                ksession.insert(message);

                // Fire the rule engine.
                try {
                    ksession.fireAllRules();
                } catch (Throwable exception) {
                    // We will catch all exceptions and proceed.
                    // We do not want to pay the price for exception in user rule.
                    String excption_msg = LogFormatter("Policyengine", Action.DENY,
                                                   message.getSrcUuid(),
                                                   message.getDstUuid(),
                                                   "",
                                                   "Packet processing skipped due to exception raised by rule engine");
                    Log(Level.INFO, excption_msg);
                    System.out.println(exception);
                    continue;
                }

                if (message.getAction() == Action.ACCEPT && message.getOutputTopic() != "") {
                    ProducerRecord<String, String> data = new ProducerRecord<String, String>(message.getOutputTopic(), "key-" + record.key(), "message-"+record.value());
                   producer.send(data, callback);
                } else if (message.getAction() == Action.IMPLICITDENY) {
                    String drop_msg = LogFormatter("Policyengine", Action.IMPLICITDENY,
                                                   message.getSrcUuid(),
                                                   message.getDstUuid(),
                                                   "",
                                                   "Dropped by policyengine due to implicit deny");
                    Log(Level.INFO, drop_msg);

                } else if (message.getAction() == Action.DENY) {
                    String drop_msg = LogFormatter("Policyengine", Action.DENY,
                                                   message.getSrcUuid(),
                                                   message.getDstUuid(),
                                                   "",
                                                   "Dropped by deny action by rule engine");
                    Log(Level.INFO, drop_msg);
                } else if (message.getOutputTopic() == "") {
                    String drop_msg = LogFormatter("Policyengine", Action.DENY,
                                                   message.getSrcUuid(),
                                                   message.getDstUuid(),
                                                   "",
                                                   "Dropped by policyengine as output topic is unknown");
                    Log(Level.INFO, drop_msg);
                }
            }
        }
    }

    private static class  TestConsumerRebalanceListener implements ConsumerRebalanceListener {
        @Override
        public void onPartitionsRevoked(Collection<TopicPartition> partitions) {
            System.out.println("Called onPartitionsRevoked with partitions:" + partitions);
        }

        @Override
        public void onPartitionsAssigned(Collection<TopicPartition> partitions) {
            System.out.println("Called onPartitionsAssigned with partitions:" + partitions);
        }
    }

    private static class TestCallback implements Callback {
        @Override
        public void onCompletion(RecordMetadata recordMetadata, Exception e) {
            if (e != null) {
                System.out.println("Error while producing message to topic :" + recordMetadata);
                e.printStackTrace();
            } else {
                String message = String.format("sent message to topic:%s partition:%s  offset:%s", recordMetadata.topic(), recordMetadata.partition(), recordMetadata.offset());
                System.out.println(message);
            }
       }
    }
}

