package com.apeiro.app;

import java.util.HashMap;
import com.google.gson.internal.LinkedTreeMap;
import java.lang.*;
import com.apeiro.app.Action;

public class Message {

    private Action action = Action.IMPLICITDENY;
    private boolean notify = false;
    private LinkedTreeMap<String,Object> data;
    private String output_topic;
    String src_uuid;
    String dst_uuid;
    LinkedTreeMap<String, Object> md;

    // Tags.
    HashMap<String, String> networkFromTag;
    HashMap<String, String> deviceFromTag;
    HashMap<String, String> serviceFromTag;

    HashMap<String, String> networkToTag;
    HashMap<String, String> deviceToTag;
    HashMap<String, String> serviceToTag;

    public Message(String default_output_topic) {
        this.notify = false;
        this.output_topic = default_output_topic;
        this.networkFromTag = new HashMap<String, String>();
        this.deviceFromTag= new HashMap<String, String>();
        this.serviceFromTag= new HashMap<String, String>();
        this.networkToTag= new HashMap<String, String>();
        this.deviceToTag= new HashMap<String, String>();
        this.serviceToTag= new HashMap<String, String>();
        this.src_uuid = new String("unknown");
        this.dst_uuid = new String("unknown");
    }

    public void setData(Object input_data) {
        this.data = (LinkedTreeMap<String, Object>)input_data;
        this.setMetadata(this.data.get("md"));
    }

    public void setMetadata(Object md) {
        this.md = (LinkedTreeMap<String, Object>)md;

        if (this.md != null && this.md.containsKey("src_endpoint_id")) {
            this.src_uuid = (String)this.md.get("src_endpoint_id");
        }
        if (this.md != null && this.md.containsKey("dst_endpoint_id")) {
            this.dst_uuid = (String)this.md.get("dst_endpoint_id");
        }
    }

    public void setNetworkFromTag(HashMap<String, String> tags) {
        this.networkFromTag = tags;
    }

    public HashMap<String, String> getNetworkFromTag() {
        return this.networkFromTag;
    }

    public void setDeviceFromTag(HashMap<String, String> tags) {
        this.deviceFromTag = tags;
    }

    public HashMap<String, String> getDeviceFromTag() {
        return this.deviceFromTag;
    }

    public void setServiceFromTag(HashMap<String, String> tags) {
        this.serviceFromTag = tags;
    }

    public HashMap<String, String> getServiceFromTag() {
        return this.serviceFromTag;
    }

    public void setNetworkToTag(HashMap<String, String> tags) {
        this.networkToTag = tags;
    }

    public HashMap<String, String> getNetworkToTag() {
        return this.networkToTag;
    }

    public void setDeviceToTag(HashMap<String, String> tags) {
        this.deviceToTag = tags;
    }

    public HashMap<String, String> getDeviceToTag() {
        return this.deviceToTag;
    }

    public void setServiceToTag(HashMap<String, String> tags) {
        this.serviceToTag = tags;
    }

    public HashMap<String, String> getServiceToTag() {
        return this.serviceToTag;
    }

    public String getSrcUuid() {
        return this.src_uuid;
    }

    public String getDstUuid() {
        return this.dst_uuid;
    }

    public LinkedTreeMap<String,Object> getData() {
        return this.data;
    }

    public void setNotify(boolean v) {
        if (this.notify == true && v == false) {
            this.notify = false;
        }
    }

    public boolean getNotify() {
        return this.notify;
    }

    public void setAction(Action action) {
        if (this.action == Action.DENY) {
            // If action is already set to DENY, then it cannot be overriden.
            return;
        }
        this.action = action;
    }

    public Action getAction() {
        return this.action;
    }

    public String getOutputTopic() {
        return this.output_topic;
    }

    public void setOutputTopic(String topic) {
        this.output_topic = topic;
    }
}


