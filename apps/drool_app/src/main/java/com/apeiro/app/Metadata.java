package com.apeiro.app;

import com.google.gson.internal.LinkedTreeMap;
import java.lang.*;

public class Metadata {

    LinkedTreeMap<String, Object> md;
    String uuid;

    public Metadata() {
    }

    public void setMetadata(Object md) {
        this.md = (LinkedTreeMap<String, Object>)md;
        this.uuid = (String)this.md.get("uuid");
    }

    public LinkedTreeMap<String,Object> getMetadata() {
        return this.md;
    }

    public String getUuid() {
        return this.uuid;
    }
}

