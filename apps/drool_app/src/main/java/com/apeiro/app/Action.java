package com.apeiro.app;

public enum Action {
    IMPLICITDENY,
    ACCEPT,
    DENY,
    NOTIFY
}
