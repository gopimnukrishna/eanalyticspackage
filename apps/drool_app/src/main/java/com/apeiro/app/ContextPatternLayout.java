package com.apeiro.app;

import java.net.InetAddress;
import java.net.UnknownHostException;

import org.apache.log4j.helpers.PatternConverter;
import org.apache.log4j.helpers.PatternParser;
import org.apache.log4j.PatternLayout;
import org.apache.log4j.spi.LoggingEvent;


/**
 * Context pattern layout with %h (hostname) and %u (username) variables.
 * 
 * @author Lukasz Frankowski (http://lifeinide.com)
 */
public class ContextPatternLayout extends PatternLayout {

  protected String host;

  protected String getUsername() {
    return "username";
  }

  protected String getHostname() {
    if (host==null) {
      try {
        InetAddress addr = InetAddress.getLocalHost();
        this.host = addr.getHostName();
      } catch (UnknownHostException e) {
        this.host = "localhost";
      }
   }
   return host;
 }

 @Override
 protected PatternParser createPatternParser(String pattern) {
   return new PatternParser(pattern) {

     @Override
     protected void finalizeConverter(char c) {
       PatternConverter pc = null;

       switch (c) {
         case 'u':
           pc = new PatternConverter() {
             @Override
             protected String convert(LoggingEvent event) {
               return getUsername();
             }
           };
           break;

         case 'h': 
           pc = new PatternConverter() {
             @Override
             protected String convert(LoggingEvent event) {
               return getHostname();
             }
           };
           break;
       }

       if (pc==null)
         super.finalizeConverter(c);
       else
         addConverter(pc);
     }
   };
 }
}
