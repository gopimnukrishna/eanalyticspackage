#!/bin/bash

# Argument parsing.
if [ ! -z "$1" ]; then
    SERVER_IP=$1
fi

if [ ! -z "$2" ]; then
    SERVER_PORT=$2
fi

if [ ! -z "$3" ]; then
    AUDISP_REMOTE_TEMPLATE=$3
fi

: "${SERVER_IP:?"Need to provide arg 1: SERVER_IP"}"
: "${SERVER_PORT:?"Need to provide arg 2: SERVER_PORT"}"
: "${AUDISP_REMOTE_TEMPLATE:?"Need to provide arg 3: AUDISP_REMOTE_TEMPLATE"}"

# Install auditd and its dispatch plugin.
sudo apt-get install -y auditd audispd-plugins

# Determine the current directory.
curr_dir="${BASH_SOURCE%/*}"

# Construct and copy over audisp-remote.conf
IFS=
conf=$(sed -e 's/"/\\"/g' -e 's/\$/uUu/g' -e 's/{{ *\([^ }]\+\) *}}/$\1/g' $AUDISP_REMOTE_TEMPLATE)
eval echo \"$conf\" | sed 's/uUu/$/g' > /etc/audisp/audisp-remote.conf

# Construct and copy over au-remote.conf
sudo cp $curr_dir/au-remote.conf.template /etc/audisp/plugins.d/au-remote.conf

# Restart auditd service.
sudo service auditd restart
